package imlv.vn.real.services;

import com.atlassian.activeobjects.external.ActiveObjects;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import imlv.vn.real.models.ManualIncludePage;
import java.util.List;
import javax.inject.Inject;
import net.java.ao.Query;

public class ManualIncludePageServiceImpl implements ManualIncludePageService {

    private final ActiveObjects ao;

    @Inject
    public ManualIncludePageServiceImpl(ActiveObjects ao) {
        this.ao = checkNotNull(ao);
    }

    @Override
    public List<ManualIncludePage> getAll() {
        return newArrayList(ao.find(ManualIncludePage.class, Query.select().where("Delete_Flag = ?", false)));
    }

//    @Override
//    public List<ManualIncludePage> search(String searchKey) {
//        try {
//            StringBuilder strBuilder = new StringBuilder();
//            strBuilder.append("ManualVersionKey Like ? or");
//            strBuilder.append("ManualVersion Like ? or");
//            strBuilder.append("PageID Like ? or");
//            strBuilder.append("PageVersion Like ? or");
//            strBuilder.append("IncludePageID Like ? or");
//            strBuilder.append("IncludePageVersion Like ? or");
//            return newArrayList(ao.find(ManualIncludePage.class, Query.select().
//                    where(strBuilder.toString(),
//                            "%" + searchKey + "%", "%" + searchKey + "%", "%" + searchKey + "%",
//                            "%" + searchKey + "%", "%" + searchKey + "%", "%" + searchKey + "%")));
//        } catch (Exception e) {
//            throw new RuntimeException();
//        }
//    }

    @Override
    public ManualIncludePage add(int manualVersionKey, String manualVersion, long pageID, int pageVersion, long includePageID, int includePageVersion) {
        try {
            final ManualIncludePage manualIncludePage = ao.create(ManualIncludePage.class);
            manualIncludePage.setManualVersionKey(manualVersionKey);
            manualIncludePage.setManualVersion(manualVersion);
            manualIncludePage.setPageID(pageID);
            manualIncludePage.setPageVersion(pageVersion);
            manualIncludePage.setIncludePageID(includePageID);
            manualIncludePage.setIncludePageVersion(includePageVersion);
            manualIncludePage.setDeleteFlag(false);
            manualIncludePage.save();
            return manualIncludePage;
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

    @Override
    public ManualIncludePage edit(ManualIncludePage manualIncludePage) {
        try {
            final ManualIncludePage manualIncludePageUpdate = ao.get(ManualIncludePage.class, manualIncludePage.getManualVersionKey());
            manualIncludePageUpdate.setManualVersionKey(manualIncludePage.getManualVersionKey());
            manualIncludePageUpdate.setManualVersion(manualIncludePage.getManualVersion());
            manualIncludePageUpdate.setPageID(manualIncludePage.getPageID());
            manualIncludePageUpdate.setPageVersion(manualIncludePage.getPageVersion());
            manualIncludePageUpdate.setIncludePageID(manualIncludePage.getIncludePageID());
            manualIncludePageUpdate.setIncludePageVersion(manualIncludePage.getIncludePageVersion());
            manualIncludePageUpdate.setDeleteFlag(manualIncludePage.isDeleteFlag());
            manualIncludePageUpdate.save();
            return null;
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

    @Override
    public void delete(ManualIncludePage manualIncludePage) {
        try {
            ao.delete(manualIncludePage);
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

}
