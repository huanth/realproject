package imlv.vn.real.services;

import imlv.vn.real.models.ManualDetail;
import java.util.List;

public interface ManualDetailService {

    List<ManualDetail> getAll();

//    List<ManualDetail> search(String searchKey);

    ManualDetail add(int manualVersionKey, String manualVersion, long pageID, int pageVersion, String pageTitle, int parentPageID, int position);

    ManualDetail edit(ManualDetail manualDetail);

    void delete(ManualDetail manualDetail);
}
