package imlv.vn.real.models;

import net.java.ao.Entity;

public interface ManualIncludePage extends Entity {

    int getManualVersionKey();

    void setManualVersionKey(int ManualVersionKey);

    String getManualVersion();

    void setManualVersion(String ManualVersion);

    Long getPageID();

    void setPageID(Long PageID);

    int getPageVersion();

    void setPageVersion(int PageVersion);

    Long getIncludePageID();

    void setIncludePageID(Long IncludePageID);

    int getIncludePageVersion();

    void setIncludePageVersion(int IncludePageVersion);

    boolean isDeleteFlag();

    void setDeleteFlag(boolean DeleteFlag);

}
