package imlv.vn.real.models;

import net.java.ao.Entity;

public interface ManualAttachment extends Entity {

    int getManualVersionKey();

    void setManualVersionKey(int ManualVersionKey);

    String getManualVersion();

    void setManualVersion(String ManualVersion);

    Long getPageID();

    void setPageID(Long PageID);

    int getPageVersion();

    void setPageVersion(int PageVersion);

    Long getAttachmentID();

    void setAttachmentID(Long AttachmentID);

    int getAttachmentVersion();

    void setAttachmentVersion(int AttachmentVersion);

    boolean isDeleteFlag();

    void setDeleteFlag(boolean DeleteFlag);
}
