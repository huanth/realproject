package imlv.vn.real;

import com.atlassian.confluence.core.Beanable;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import static com.opensymphony.xwork.Action.SUCCESS;
import com.opensymphony.xwork.ActionContext;
import imlv.vn.real.business.BusinessManualVersionAction;
import imlv.vn.real.models.ManualManagement;
import imlv.vn.real.models.ManualVersion;
import imlv.vn.real.util.ActionContextSupport;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ManualVersionAction extends ConfluenceActionSupport implements Beanable {

    Map<String, Object> map = new HashMap<String, Object>();
    private Set<String> listStr;
    private String renderTableManualManagement;
    private String url;

    @Override
    public Object getBean() {
        return map;
    }

    public String goToMenuRedirect() {
        listStr = new HashSet<String>();
        BusinessManualVersionAction businessManualVersionAction = new BusinessManualVersionAction();
        listStr = businessManualVersionAction.getAllCategoriesInManualVersion();
        return SUCCESS;
    }

    public String loadCategory() {
        BusinessManualVersionAction businessManualVersionAction = new BusinessManualVersionAction();
        listStr = new HashSet<String>();
        listStr = businessManualVersionAction.getAllCategoriesInManualVersion();
        map.put("categories", new ArrayList<String>(listStr));
        return SUCCESS;
    }

    public String filterByCategory() {
        BusinessManualVersionAction businessManualVersionAction = new BusinessManualVersionAction();
        ActionContext context = ActionContext.getContext();
        String category = ActionContextSupport.getParameterValue(context, "category");

        List<ManualManagement> listManualManagement = businessManualVersionAction.getManualManagementByCategory(category);
        List<ManualVersion> listManualVersion = businessManualVersionAction.getAllManualVersion();

        Map<String, List<? extends Object>> mapContext = new HashMap<String, List<? extends Object>>();
        mapContext.put("listManualManagement", listManualManagement);
        mapContext.put("listManualVersion", listManualVersion);
        renderTableManualManagement = VelocityUtils.getRenderedTemplate("/view/TableManualManagement.vm", mapContext);
        map.put("tbodytable", renderTableManualManagement);

        return SUCCESS;
    }

    public String createNewManual() {
        BusinessManualVersionAction businessManualVersionAction = new BusinessManualVersionAction();

        ActionContext context = ActionContext.getContext();
        String manualName = ActionContextSupport.getParameterValue(context, "manualname");
        String productName = ActionContextSupport.getParameterValue(context, "productname");
        String versionDisplay = ActionContextSupport.getParameterValue(context, "versiondisplay");
        String[] createCategories = ActionContextSupport.getParameterValues(context, "createcategories");
        String spaceKey = ActionContextSupport.getNow();

        String result = businessManualVersionAction.createNewManual(manualName, productName, versionDisplay, createCategories, spaceKey);
        if (result.equals("error")) {
            return ERROR;
        } else {
            url = result;
        }
        return SUCCESS;
    }

    public String releaseManual() {
        BusinessManualVersionAction businessManualVersionAction = new BusinessManualVersionAction();
        ActionContext context = ActionContext.getContext();
        String manualVersionKeyContext = ActionContextSupport.getParameterValue(context, "manualVersionKey");
        String manualVersionContext = ActionContextSupport.getParameterValue(context, "manualVersion");
        String currentCategory = ActionContextSupport.getParameterValue(context, "currentCategory");
        int manualVerisonKey = Integer.parseInt(manualVersionKeyContext);

        int result = businessManualVersionAction.releaseVersion(manualVerisonKey, manualVersionContext);
        if (result == -1) {
            map.put("error", "Please Create Some Page Now");
            return SUCCESS;
        }

        List<ManualManagement> listManualManagement = businessManualVersionAction.getManualManagementByCategory(currentCategory);
        List<ManualVersion> listManualVersion = businessManualVersionAction.getAllManualVersion();

        Map<String, List<? extends Object>> mapContext = new HashMap<String, List<? extends Object>>();
        mapContext.put("listManualManagement", listManualManagement);
        mapContext.put("listManualVersion", listManualVersion);
        renderTableManualManagement = VelocityUtils.getRenderedTemplate("/view/TableManualManagement.vm", mapContext);
        map.put("error", "");
        map.put("tbodytable", renderTableManualManagement);
        return SUCCESS;
    }

    public String revisionUpManual() {
        BusinessManualVersionAction businessManualVersionAction = new BusinessManualVersionAction();
        //get input data
        ActionContext context = ActionContext.getContext();
        String manualVersionKeyContext = ActionContextSupport.getParameterValue(context, "manualVersionKey");
        String currentCategory = ActionContextSupport.getParameterValue(context, "currentCategory");
        String manualVersionContext = ActionContextSupport.getParameterValue(context, "manualVersion");
        String manualVersionDisplay = ActionContextSupport.getParameterValue(context, "manualVersionDisplay");
        int manualVerisonKey = Integer.parseInt(manualVersionKeyContext);
        //handler revisionup
        businessManualVersionAction.revisionUpVersion(manualVerisonKey, manualVersionContext, manualVersionDisplay);
        //only load new data in table
        List<ManualManagement> listManualManagement = businessManualVersionAction.getManualManagementByCategory(currentCategory);
        List<ManualVersion> listManualVersion = businessManualVersionAction.getAllManualVersion();
        Map<String, List<? extends Object>> mapContext = new HashMap<String, List<? extends Object>>();
        mapContext.put("listManualManagement", listManualManagement);
        mapContext.put("listManualVersion", listManualVersion);
        renderTableManualManagement = VelocityUtils.getRenderedTemplate("/view/TableManualManagement.vm", mapContext);
        map.put("error", "");
        map.put("tbodytable", renderTableManualManagement);
        return SUCCESS;
    }

    public void setListStr(Set<String> listStr) {
        this.listStr = listStr;
    }

    public Set<String> getListStr() {
        return listStr;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
