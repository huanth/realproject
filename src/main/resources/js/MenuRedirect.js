/* global AJS */
AJS.toInit(function () {
    $('ul#manual-management-nav-categories li').click(function () {
        $('ul#manual-management-nav-categories li.aui-nav-selected').removeClass('aui-nav-selected');
        $(this).addClass('aui-nav-selected');
        var category = $(this).children(".hidden").text();
        $.ajax({
            async: false,
            type: 'GET',
            url: AJS.contextPath() + "/plugins/real/search-by-category.action",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: {
                category: category
            },
            success: function (returnData) {
                $('#manual-management-table-tbody').html(returnData.tbodytable);
            },
            error: function (returnData) {
                alert('error r');
            }
        });
    });

    $(window).on('load', function () {
        $('ul#manual-management-nav-categories li:first-child').trigger('click');
    });

    $('#manual-management-table-tbody').on('click', 'a.plugin_pagetree_childtoggle', function () {
        $(this).toggleClass('icon-section-closed icon-section-opened');
        $(this).parent().parent().nextUntil('tr.table-manual-management-table-tr').slideToggle(10, function () {});
        if ($(this).hasClass('icon-section-opened')) {
            $(this).parent().parent().addClass('tr-is-choosen');
        } else {
            $(this).parent().parent().removeClass('tr-is-choosen');
        }
    });

    $('#manual-management-table-tbody').on('click', 'button.show-confirm-release-button', function () {
        $(this).parent().parent().addClass("is-clicked");
        AJS.dialog2('#confirm-release-dialog').show();
        var manualVersionKey = $(this).parent().find('.manualVersionKey').val();
        var manualVersion = $(this).parent().find('.manualVersion').val();
        $('#confirm-release-dialog').find('.manualVersionKey').val(manualVersionKey);
        $('#confirm-release-dialog').find('.manualVersion').val(manualVersion);
    });

    $('#confirm-release-dialog button.confirm-release-no-button').on('click', function () {
        $('tbody#manual-management-table-tbody').find('tr.is-clicked').removeClass('is-clicked');
        AJS.dialog2('#confirm-release-dialog').hide();
    });

    $('#confirm-release-dialog button.confirm-release-yes-button').on('click', function () {
        var manualVersionKey = $('#confirm-release-dialog').find('.manualVersionKey').val();
        var manualVersion = $('#confirm-release-dialog').find('.manualVersion').val();
        var currentCategory = $('ul#manual-management-nav-categories li.aui-nav-selected').children('span.hidden').text();
//        alert('inside');
        $.ajax({
            async: false,
            type: 'GET',
            url: AJS.contextPath() + "/plugins/real/release.action",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: {
                manualVersionKey: manualVersionKey,
                manualVersion: manualVersion,
                currentCategory: currentCategory
            },
            success: function (returnData) {
                var mess = '';
                if (returnData.error.toString() <= 0) {
                    var tr_choosen = $('#manual-management-table-tbody tr.tr-is-choosen');
                    var tr_version_choosen = $('#manual-management-table-tbody tr.is-clicked');
                    $('#manual-management-table-tbody').html(returnData.tbodytable);
                    $.each($('#manual-management-table-tbody tr.table-manual-management-table-tr'), function (index, value) {
                        var currentTr = $(this).find('.plugin_pagetree_childtoggle');
                        var currentTr_text = $(this).find('.link-to-home-page').text();
                        $.each(tr_choosen, function (index, value) {
                            if (currentTr_text === $(this).find('.link-to-home-page').text()) {
                                currentTr.trigger('click');
                            }
                        });
                    });
                    $('#message-handler-function').text('');
                    mess = '<div class="aui-message aui-message-success">\n\
                                <p class="title">\n\
                                    <strong>Success!</strong>\n\
                                </p>\n\
                                <p>' + tr_version_choosen.find('.link-version-display').text() + ' Has Been Realease. \n' +
                            '</p>\n\
                            </div>';
                } else {
                    $('#message-handler-function').text('');
                    mess = '<div class="aui-message aui-message-error">\n\
                                <p class="title">\n\
                                    <strong>Error!</strong>\n\
                                </p>\n\
                                <p>' + returnData.error +
                            '</p>\n\
                            </div>';
                }

                $('#message-handler-function').append(mess);
            },
            error: function (request, status, error) {
                $('#message-handler-function').text('');
                var mess = '<div class="aui-message aui-message-error">\n\
                                <p class="title">\n\
                                    <strong>Error System!</strong>\n\
                                </p>\n\
                                <p>' + error + 'And this is just content in a Default message.\n' +
                        '</p>\n\
                            </div>';
                $('#message-handler-function').append(mess);
            }
        });
        $('#confirm-release-dialog button.confirm-release-no-button').trigger('click');
    });

    AJS.dialog2("#confirm-release-dialog").on("hide", function () {
        $('#confirm-release-dialog button.confirm-release-no-button').trigger('click');
    });

    $('#manual-management-table-tbody').on('change', 'input.checkbox-version-compare', function () {
        var button_contain = $(this).parent().parent().find('div.revision-version-button-contain');
        button_contain.toggleClass('hidden ');
    });

    $('#manual-management-table-tbody').on('click', 'button.show-confirm-revision-button', function () {
        $(this).parent().parent().parent().addClass("version-is-clicked");
        $('input#version-revisionup').val('');
        AJS.dialog2('#confirm-revision-dialog').show();
    });

    $('#revisionup-dialog-yes-button').click(function () {
        var manualVersionKey = $('tbody#manual-management-table-tbody').find('tr.version-is-clicked .manualVersionKey').val();
        var manualVersion = $('tbody#manual-management-table-tbody').find('tr.version-is-clicked .manualVersion').val();
        var manualVersionDisplay = $('#version-revisionup').val();
        var currentCategory = $('ul#manual-management-nav-categories li.aui-nav-selected').children('span.hidden').text();
        $.ajax({
            async: false,
            type: 'GET',
            url: AJS.contextPath() + "/plugins/real/revision-up.action",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: {
                manualVersionKey: manualVersionKey,
                manualVersion: manualVersion,
                manualVersionDisplay: manualVersionDisplay,
                currentCategory: currentCategory
            },
            success: function (returnData) {
                var mess = '';

                if (returnData.error.toString() <= 0) {
                    var tr_choosen = $('#manual-management-table-tbody tr.tr-is-choosen');
                    $('#manual-management-table-tbody').html(returnData.tbodytable);
                    $.each($('#manual-management-table-tbody tr.table-manual-management-table-tr'), function (index, value) {
                        var currentTr = $(this).find('.plugin_pagetree_childtoggle');
                        var currentTr_text = $(this).find('.link-to-home-page').text();
                        $.each(tr_choosen, function (index, value) {
                            if (currentTr_text === $(this).find('.link-to-home-page').text()) {
                                currentTr.trigger('click');
                            }
                        });
                    });
                    $('#message-handler-function').text('');
                    mess = '<div class="aui-message aui-message-success">\n\
                                <p class="title">\n\
                                    <strong>Success!</strong>\n\
                                </p>\n\
                                <p>' + $('#manual-management-table-tbody tr.is-clicked a.link-version-display').text() + ' Revision Up Success. \n' +
                            '</p>\n\
                            </div>';
                } else {
                    $('#message-handler-function').text('');
                    mess = '<div class="aui-message aui-message-error">\n\
                                <p class="title">\n\
                                    <strong>Error!</strong>\n\
                                </p>\n\
                                <p>' + returnData.error +
                            '</p>\n\
                            </div>';
                }

                $('#message-handler-function').append(mess);
            },
            error: function (returnData) {
                alert('error');
            }
        });
        $('#revisionup-dialog-no-button').trigger('click');
    });

    $('#revisionup-dialog-no-button').click(function () {
        $('tbody#manual-management-table-tbody').find('tr.version-is-clicked').removeClass('version-is-clicked');
        AJS.dialog2('#confirm-revision-dialog').hide();
    });

    AJS.dialog2("#confirm-revision-dialog").on("hide", function () {
        $('#revisionup-dialog-no-button').trigger('click');
    });

    $('#manual-management-table-tbody').on('change', 'input.checkbox-version-compare', function () {
        var frist_check = $('#manual-management-table-tbody .checkbox-frist');
        var second_check = $('#manual-management-table-tbody .checkbox-second');
        var count_checkbox = $('#manual-management-table-tbody input.checkbox-version-compare').filter(':checked');
        if (this.checked) {
            var currentCheckbox = $(this);
            var flag = true;
            $.each(count_checkbox, function (index, value) {
                if (currentCheckbox.prop('id') !== $(this).prop('id')) {
                    flag = false;
                }
            });
            if (flag === true) {
                $(this).parent().parent().addClass('tr-checkbox-was-checked');
                if (count_checkbox.length <= 1) {
                    $(this).addClass('checkbox-frist');
                }
                if (count_checkbox.length < 2) {
                    $('#button-compare-version').prop('disabled', true);
                }
                if (count_checkbox.length > 2) {
                    frist_check.prop('checked', false);
                    frist_check.removeClass('checkbox-frist');
                    frist_check.parent().parent().removeClass('tr-checkbox-was-checked');
                    second_check.removeClass('checkbox-second');
                    second_check.addClass('checkbox-frist');
                }
                if (count_checkbox.length > 1) {
                    $(this).addClass('checkbox-second');
                }
            } else {
                frist_check.removeClass('checkbox-frist');
                frist_check.prop('checked', false);
                second_check.removeClass('checkbox-second');
                second_check.prop('checked', false);
                $(this).addClass('checkbox-frist');
                $(this).prop('checked', true);
            }
        } else {
            if ($(this).hasClass('checkbox-second')) {
                $(this).removeClass('checkbox-second');
                $(this).parent().parent().removeClass('tr-checkbox-was-checked');
            }
            if ($(this).hasClass('checkbox-frist')) {
                $(this).parent().parent().removeClass('tr-checkbox-was-checked');
                $(this).removeClass('checkbox-frist');
                second_check.removeClass('checkbox-second');
                second_check.addClass('checkbox-frist');


            }
        }
        count_checkbox = $('#manual-management-table-tbody input.checkbox-version-compare').filter(':checked');
        if (count_checkbox.length > 1) {
            $('#button-compare-version').prop('disabled', false);
        } else {
            $('#button-compare-version').prop('disabled', true);
        }
    });
});


