package imlv.vn.real.business;

import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import imlv.vn.real.factory.ManualVersionFactory;
import imlv.vn.real.models.ManualAttachment;
import imlv.vn.real.models.ManualDetail;
import imlv.vn.real.models.ManualHistory;
import imlv.vn.real.models.ManualIncludePage;
import imlv.vn.real.models.ManualManagement;
import imlv.vn.real.models.ManualVersion;
import imlv.vn.real.services.ManualAttachmentService;
import imlv.vn.real.services.ManualDetailService;
import imlv.vn.real.services.ManualHistoryService;
import imlv.vn.real.services.ManualIncludePageService;
import imlv.vn.real.services.ManualManagementService;
import imlv.vn.real.services.ManualVersionService;
import imlv.vn.real.util.StringHtmlEscapeUtil;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class BusinessManualVersionAction {

    ManualManagementService manualManagementService;
    ManualVersionService manualVersionService;
    ManualAttachmentService manualAttachmentService;
    ManualIncludePageService manualIncludePageService;
    ManualDetailService manualDetailService;
    ManualHistoryService manualHistoryService;
    SpaceManager spaceManager;
    PageManager pageManager;

    public BusinessManualVersionAction() {
        this.manualManagementService = ManualVersionFactory.getManualManagementService();
        this.manualVersionService = ManualVersionFactory.getManualVersionService();
        this.manualAttachmentService = ManualVersionFactory.getManualAttachmentService();
        this.manualIncludePageService = ManualVersionFactory.getManualIncludePageService();
        this.manualDetailService = ManualVersionFactory.getManualDetailService();
        this.manualHistoryService = ManualVersionFactory.getManualHistoryService();
        this.spaceManager = ManualVersionFactory.getSpaceManager();
        this.pageManager = ManualVersionFactory.getPageManager();
    }

    public void revisionUpVersion(int manualVersionKey, String manualVersion, String manualVersionDisplay) {
        //get ManualVersion want to revision up
        ManualVersion currentManualVersion = getManualVersion(manualVersionKey, manualVersion);
        //get space ManualVersion to get pages, attachments, includepages
        Space space = spaceManager.getSpace(currentManualVersion.getSpaceKey());

        String revisionManualVersion = revisionUpVersion(manualVersionKey);
        if (manualVersionDisplay.length() == 0) {
            manualVersionDisplay = revisionUpVersionDispaly(manualVersionKey);
        }
        //add Revision up in db
        addManualVersionByRevisionUp(manualVersionKey, revisionManualVersion, manualVersionDisplay, manualVersion);

        List<Page> listPage = pageManager.getPages(space, true);
        List<Attachment> listAttachment;

        for (Page page : listPage) {
            //check not Homepage
            if (page.getParent() == null) {
                continue;
            }
            Integer currentPosition = (page.getPosition() != null) ? page.getPosition() : setPositionPage(page);// index in pagetree
            Integer parentCurrentPageID = (int) page.getParent().getId();// id of parent page, Home hasn't parent
            long currentPageID = page.getId();
            int currentPageVersion = page.getVersion();
            String currentPageTitle = page.getTitle();
            //insert manual detail
            addDetailForVersion(manualVersionKey, revisionManualVersion, currentPageID, currentPageVersion, currentPageTitle, parentCurrentPageID, currentPosition);
            //insert manual attachment
            listAttachment = page.getAttachments();
            for (Attachment attachment : listAttachment) {
                addAttachmentForVersion(manualVersionKey, revisionManualVersion, currentPageID, currentPageVersion, attachment.getId(), attachment.getVersion());
            }
            //insert manual include page
            insertDBIncludePage(page, manualVersionKey, revisionManualVersion);
        }
        //isnert manual history
        //check listPage null thi version hien tai cung k co page 
        List<ManualDetail> listManualDetailBase = searchDetailByVersion(manualVersionKey, "100");
        List<ManualDetail> listManualDetailOld = searchDetailByVersion(manualVersionKey, manualVersion);
        for (ManualDetail manualDetailBase : listManualDetailBase) {
            for (ManualDetail manualDetailOld : listManualDetailOld) {
                addManualHistory(manualVersionKey, "100", manualDetailBase.getPageID(), manualDetailBase.getPageVersion(), manualVersion, manualDetailOld.getPageID(), manualDetailOld.getPageVersion(), revisionManualVersion, manualDetailOld.getPageID(), manualDetailOld.getPageVersion());
            }
        }
    }

    public int releaseVersion(int manualVersionKey, String manualVersion) {
        //get ManualVersion want to release
        ManualVersion currentManualVersion = getManualVersion(manualVersionKey, manualVersion);
        //get space of ManualVersion to get pages, attachments, includepages
        Space space = spaceManager.getSpace(currentManualVersion.getSpaceKey());
        //release
        List<Attachment> listAttachment;

        List<ManualIncludePage> listManualIncludePages = searchIncludePageByVersion(manualVersionKey, manualVersion);
        if (!listManualIncludePages.isEmpty()) {
            delManualIncludePageInDB(listManualIncludePages);
        }

        List<ManualAttachment> listManualAttachment = searchAttachmentByVersion(manualVersionKey, manualVersion);
        if (!listManualAttachment.isEmpty()) {
            delManualAttachmentInDB(listManualAttachment);
        }

        List<ManualDetail> listManualDetail = searchDetailByVersion(manualVersionKey, manualVersion);
        if (!listManualDetail.isEmpty()) {
            for (ManualDetail manualDetail : listManualDetail) {
                // update verion of Page Include 
                // get page from page id in db
                Page curPage = pageManager.getPage(manualDetail.getPageID());
                // change lastest version page
                manualDetail.setPageVersion(curPage.getVersion());
                // update changging to db
                updateManualDetail(manualDetail);

                // insert pageinclude of current page to db
                insertDBIncludePage(curPage, manualVersionKey, manualVersion);

                // insert attachment of current page to db
                listAttachment = curPage.getAttachments();
                for (Attachment attachment : listAttachment) {
                    addAttachmentForVersion(manualVersionKey, manualVersion, curPage.getId(), curPage.getVersion(), attachment.getId(), attachment.getVersion());
                }
            }
        } else {
            List<Page> listPage = pageManager.getPages(space, true);
            for (Page page : listPage) {
                // not to create page
                if (listPage.size() < 2) {
                    return -1;
                }
                //check not Homepage
                if (page.getParent() == null) {
                    continue;
                }
                Integer currentPosition = (page.getPosition() != null) ? page.getPosition() : setPositionPage(page);// index in pagetree
                Integer parentCurrentPageID = (int) page.getParent().getId();// id of parent page, Home hasn't parent
                long currentPageID = page.getId();
                int currentPageVersion = page.getVersion();
                String currentPageTitle = page.getTitle();

                addDetailForVersion(manualVersionKey, manualVersion, currentPageID, currentPageVersion, currentPageTitle, parentCurrentPageID, currentPosition);

                // insert pageinclude of current page to db
                insertDBIncludePage(page, manualVersionKey, manualVersion);

                // insert attachment of current page to db
                listAttachment = page.getAttachments();
                for (Attachment attachment : listAttachment) {
                    addAttachmentForVersion(manualVersionKey, manualVersion, page.getId(), page.getVersion(), attachment.getId(), attachment.getVersion());
                }
            }
        }
        // update status ManualVersion
        updateManualVersion(manualVersionKey, manualVersion);
        // isnert manual history
        List<ManualDetail> listManualDetailBase = searchDetailByVersion(manualVersionKey, "100");
        List<ManualDetail> listManualDetailOld = searchDetailByVersion(manualVersionKey, manualVersion);
        for (ManualDetail manualDetailBase : listManualDetailBase) {
            for (ManualDetail manualDetailOld : listManualDetailOld) {
                addManualHistory(manualVersionKey, "100", manualDetailBase.getPageID(), manualDetailBase.getPageVersion(), manualVersion, manualDetailOld.getPageID(), manualDetailOld.getPageVersion(), manualVersion, manualDetailOld.getPageID(), manualDetailOld.getPageVersion());
            }
        }
        return 1;
    }

    public void delManualAttachmentInDB(List<ManualAttachment> listManualAttachment) {
        for (ManualAttachment manualAttachment : listManualAttachment) {
            manualAttachmentService.delete(manualAttachment);
        }
    }

    public void delManualIncludePageInDB(List<ManualIncludePage> listManualIncludePages) {
        for (ManualIncludePage listManualIncludePage : listManualIncludePages) {
            manualIncludePageService.delete(listManualIncludePage);
        }
    }

    public Integer setPositionPage(Page page) {
        return page.getAncestors().size();
    }

    public void updateManualVersion(int manualVersionKey, String manualVersion) {
        List<ManualVersion> listManualVersion = manualVersionService.getAll();
        for (ManualVersion loopManualVersion : listManualVersion) {
            if (loopManualVersion.getManualVersionKey() == manualVersionKey && loopManualVersion.getManualVersion().equals(manualVersion)) {
                loopManualVersion.setStatus(true);
                manualVersionService.edit(loopManualVersion);
            }
        }
    }

    public void insertDBIncludePage(Page page, int manualVersionKey, String manualVersion) {
        try {
            String contentXML = page.getBodyAsString();
            StringBuffer strBuffer = new StringBuffer();
            strBuffer.append(contentXML);
            String context = StringHtmlEscapeUtil.unEscapeHtmlEntities(strBuffer);
            StringBuilder strBuilder = new StringBuilder();

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(context)));
            Element element;
            Element elementChild;

            String includeSpaceKey = page.getSpaceKey();
            String includePageTitle = "";

            NodeList nodeList = doc.getElementsByTagName("ac:structured-macro");
            for (int i = 0; i <= nodeList.getLength() - 1; i++) {
                element = (Element) nodeList.item(i);
                if (element.getAttribute("ac:name").equals("include")) {

                    NodeList lst = element.getElementsByTagName("ri:page");
                    for (int j = 0; j < lst.getLength(); j++) {
                        elementChild = (Element) lst.item(i);

                        // if macro same space
                        if (elementChild.getAttribute("ri:space-key") == null) {
                            includePageTitle = elementChild.getAttribute("ri:content-title");
                        } else {
                            //if macro another page
                            includeSpaceKey = elementChild.getAttribute("ri:space-key");
                            strBuilder.append(elementChild.getAttribute("ri:space-key"));
                            strBuilder.append(":");
                            strBuilder.append(elementChild.getAttribute("ri:content-title"));
                            includePageTitle = strBuilder.toString();
                        }
                    }
                    Page includePage = pageManager.getPage(includeSpaceKey, includePageTitle);
                    // insert data to ManualIncludePage
                    long PageID = page.getId();
                    int PageVersion = page.getVersion(); // page.version
                    long IncludePageID = includePage.getId();
                    int IncludePageVersion = includePage.getVersion();// includePage.version
                    addIncludePageForVersion(manualVersionKey, manualVersion, PageID, PageVersion, IncludePageID, IncludePageVersion);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(BusinessManualVersionAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ManualVersion getManualVersion(int manualVersionKey, String manualVersion) {
        List<ManualVersion> listManualVersion = manualVersionService.getAll();
        for (ManualVersion loopManualVersion : listManualVersion) {
            if (loopManualVersion.getManualVersionKey() == manualVersionKey && loopManualVersion.getManualVersion().equals(manualVersion)) {
                return loopManualVersion;
            }
        }
        return null;
    }

    public List<ManualDetail> searchDetailByVersion(int manualVersionKey, String manualVersionName, long pageID) {
        List<ManualDetail> listManualDetailSuccess = new ArrayList<ManualDetail>();
        List<ManualDetail> listManualDetail = manualDetailService.getAll();
        for (ManualDetail manualDetail : listManualDetail) {
            if (manualDetail.getPageID() == pageID && manualDetail.getManualVersionKey() == manualVersionKey && manualDetail.getManualVersion().equals(manualVersionName)) {
                listManualDetailSuccess.add(manualDetail);
            }
        }
        return listManualDetailSuccess;
    }

    public List<ManualDetail> searchDetailByVersion(int manualVersionKey, String manualVersionName) {
        List<ManualDetail> listManualDetailSuccess = new ArrayList<ManualDetail>();
        List<ManualDetail> listManualDetail = manualDetailService.getAll();
        for (ManualDetail manualDetail : listManualDetail) {
            if (manualDetail.getManualVersionKey() == manualVersionKey && manualDetail.getManualVersion().equals(manualVersionName)) {
                listManualDetailSuccess.add(manualDetail);
            }
        }
        return listManualDetailSuccess;
    }

    public void updateManualDetail(ManualDetail manualDetail) {
        manualDetailService.edit(manualDetail);
    }

    public void addDetailForVersion(int manualVersionKey, String manualVersion, long pageID, int pageVersion, String pageTitle, int parentPageID, int Position) {
        manualDetailService.add(manualVersionKey, manualVersion, pageID, pageVersion, pageTitle, parentPageID, Position);
    }

    public ManualIncludePage addIncludePageForVersion(int manualVersionKey, String manualVersion, long pageID, int pageVersion, long includePageID, int includePageVersion) {
        return manualIncludePageService.add(manualVersionKey, manualVersion, pageID, pageVersion, includePageID, includePageVersion);
    }

    public List<ManualIncludePage> searchIncludePageByVersion(int manualVersionKey, String manualVersion) {
        List<ManualIncludePage> listManualIncludePagesSuccess = new ArrayList<ManualIncludePage>();
        List<ManualIncludePage> listManualIncludePages = manualIncludePageService.getAll();
        for (ManualIncludePage manualIncludePage : listManualIncludePages) {
            if (manualIncludePage.getManualVersionKey() == manualVersionKey && manualIncludePage.getManualVersion().equals(manualVersion)) {
                listManualIncludePagesSuccess.add(manualIncludePage);
            }
        }
        return listManualIncludePagesSuccess;
    }

    public void addAttachmentForVersion(int manualVersionKey, String manualVersion, long pageID, int pageVersion, long attachmentID, int attachmentVersion) {
        manualAttachmentService.add(manualVersionKey, manualVersion, pageID, pageVersion, attachmentID, attachmentVersion);
    }

    public List<ManualAttachment> searchAttachmentByVersion(int manualVersionKey, String manualVersion) {
        List<ManualAttachment> listManualAttachmentSuccess = new ArrayList<ManualAttachment>();
        List<ManualAttachment> listManualAttachment = manualAttachmentService.getAll();
        for (ManualAttachment manualAttachment : listManualAttachment) {
            if (manualAttachment.getManualVersionKey() == manualVersionKey && manualAttachment.getManualVersion().equals(manualVersion)) {
                listManualAttachmentSuccess.add(manualAttachment);
            }
        }

        return listManualAttachmentSuccess;
    }

    public List<ManualVersion> searchManualVersion(int manualVersionKey, String manualVersion) {
        List<ManualVersion> listManualVersionChoosen = new ArrayList<ManualVersion>();
        List<ManualVersion> listManualVersion = manualVersionService.getAll();
        for (ManualVersion loopManualVersion : listManualVersion) {
            if (loopManualVersion.getManualVersionKey() == manualVersionKey && loopManualVersion.getManualVersion().equals(manualVersion)) {
                listManualVersionChoosen.add(loopManualVersion);
                break;
            }
        }
        return listManualVersionChoosen;
    }

    public List<ManualVersion> searchManualVersions(int manualVersionKey) {
        List<ManualVersion> listManualVersionChoosen = new ArrayList<ManualVersion>();
        for (ManualVersion loopManualVersion : manualVersionService.getAll()) {
            if (loopManualVersion.getManualVersionKey() == manualVersionKey) {
                listManualVersionChoosen.add(loopManualVersion);
            }
        }
        return listManualVersionChoosen;
    }

    public ManualVersion addManualVersionByRevisionUp(int manualVersionKey, String manualVersion, String manualVersionDisplay, String manualVersionOld) {
        List<ManualVersion> manualVersionByManualVersionKey = searchManualVersion(manualVersionKey, manualVersionOld);
        return manualVersionService.add(manualVersionKey, manualVersion, manualVersionDisplay, manualVersionByManualVersionKey.get(0).getSpaceKey());
    }

    public String revisionUpString(String manualVersion) {
        int revision = Integer.parseInt(manualVersion) + 1;
        String revisionUp = String.valueOf(revision);
        return revisionUp;
    }

    public String getManualVersionLastest(int manualVersionKey) {
        List<ManualVersion> listManualVersion = searchManualVersions(manualVersionKey);
        Collections.sort(listManualVersion, new Comparator<ManualVersion>() {
            @Override
            public int compare(ManualVersion o1, ManualVersion o2) {
                return Integer.parseInt(o2.getManualVersion()) - Integer.parseInt(o1.getManualVersion());
            }
        });
        return listManualVersion.get(0).getManualVersion();
    }

    public String revisionUpVersion(int manualVersionKey) {
        String manualVersionLastest = getManualVersionLastest(manualVersionKey);
        return revisionUpString(manualVersionLastest);
    }

    public String revisionUpVersionDispaly(int manualVersionKey) {
        String manualVersionLastest = getManualVersionLastest(manualVersionKey);
        String revisionUpManualVersionLastest = revisionUpString(manualVersionLastest);
        String revision = revisionUpManualVersionLastest.substring(revisionUpManualVersionLastest.length() - 2, revisionUpManualVersionLastest.length());
        String version = revisionUpManualVersionLastest.substring(0, revisionUpManualVersionLastest.length() - 2);
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append("V");
        strBuilder.append(version);
        strBuilder.append(".");
        strBuilder.append(revision);
        return strBuilder.toString();
    }

    public ManualHistory addManualHistory(int manualVersionKey, String baseManualVersion, long basePageID, int basePageVersion, String fromManualVersion, long fromPageID, int fromPageVersion, String toManualVersion, long toPageID, int toPageVersion) {
        return manualHistoryService.add(manualVersionKey, baseManualVersion, basePageID, basePageVersion, fromManualVersion, fromPageID, fromPageVersion, toManualVersion, toPageID, toPageVersion);
    }

    public List<ManualManagement> getManualManagementByCategory(String searchKey) {
        try {
            return manualManagementService.searchByCategory(searchKey);
        } catch (Exception e) {
            return null;
        }
    }

    public List<ManualManagement> getMultiManualManagementByCategory(String searchKey) throws Exception {
        List<ManualManagement> listManualManagementChoose = new ArrayList<ManualManagement>();
        try {
            List<ManualManagement> listManualManagement = manualManagementService.getAll();
            for (ManualManagement manualManagement : listManualManagement) {
                String category = manualManagement.getCategory();
                if (manualManagement.isDeleteFlag() == false && category.contains(searchKey)) {
                    listManualManagementChoose.add(manualManagement);
                }
            }
            return listManualManagementChoose;
        } catch (Exception e) {
            return null;
        }
    }

    public List<ManualVersion> getAllManualVersion() {
        try {
            return manualVersionService.getAll();
        } catch (Exception e) {
            return null;
        }
    }

    public void editManualVersion(ManualVersion manualVersion) throws Exception {
        try {
            manualVersionService.edit(manualVersion);
        } catch (Exception e) {
            throw new Exception();
        }
    }

    public boolean insertDatabase(String manualVersionName, String productName, String manualVersionDisplay, String category, String spaceKey) {
        try {
            int id = manualManagementService.add(manualVersionName, productName, category);
            manualVersionService.add(id, "100", manualVersionDisplay, spaceKey);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public Set<String> getAllCategoriesInManualVersion() {
        Set<String> set = new HashSet<String>();
        List<ManualManagement> listManualManagement = manualManagementService.getAll();
        for (ManualManagement manualManagement : listManualManagement) {
            String categoryStr = manualManagement.getCategory();
            boolean check = categoryStr.contains("||");
            if (!check) {
                continue;
            }
            String[] categoryStrArr = categoryStr.split("\\|\\|");
            for (String string : categoryStrArr) {
                set.add(string);
            }
        }
        return set;
    }

    public String createNewManual(String manualName, String productName, String versionDisplay, String[] createCategories, String spaceKey) {
        //create Space
        ConfluenceUser confluenceUser = AuthenticatedUserThreadLocal.get();
        Space space = new Space();
        SpaceDescription spaceDescription = new SpaceDescription();
        spaceDescription.setBodyAsString("Description ActionContextSupport Space");
        spaceDescription.setSpace(space);
        space.setName(manualName + "_V1.00");
        space.setKey(spaceKey);
        space.setCreator(confluenceUser);
        space.setDescription(spaceDescription);
        spaceManager.createSpace(space);

        //insert db ManualManagement
        StringBuilder strBuilder = new StringBuilder();
        if (createCategories.length == 0) {
            strBuilder.append("");
        } else {
            for (String createCategory : createCategories) {
                strBuilder.append(createCategory);
                strBuilder.append("||");
            }
        }
        //insert db ManualVersion
        if (versionDisplay.equals("")) {
            versionDisplay = "V1.00";
        }
        boolean check = insertDatabase(manualName, productName, versionDisplay, strBuilder.toString(), spaceKey);
        if (!check) {
            return "error";
        } else {
            return space.getUrlPath();
        }
    }

}
