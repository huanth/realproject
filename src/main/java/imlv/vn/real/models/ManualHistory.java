package imlv.vn.real.models;

import net.java.ao.Entity;

public interface ManualHistory extends Entity {

    int getManualVersionKey();

    void setManualVersionKey(int ManualVersionKey);

    String getBaseManualVersion();

    void setBaseManualVersion(String BaseManualVersion);

    Long getBasePageID();

    void setBasePageID(Long BasePageID);

    int getBasePageVersion();

    void setBasePageVersion(int BasePageVersion);

    String getFromManualVersion();

    void setFromManualVersion(String FromManualVersion);

    Long getFromPageID();

    void setFromPageID(Long FromPageID);

    int getFromPageVersion();

    void setFromPageVersion(int FromPageVersion);

    String getToManualVersion();

    void setToManualVersion(String ToManualVersion);

    Long getToPageID();

    void setToPageID(Long ToPageID);

    int getToPageVersion();

    void setToPageVersion(int ToPageVersion);

    boolean isDeleteFlag();

    void setDeleteFlag(boolean DeleteFlag);
}
