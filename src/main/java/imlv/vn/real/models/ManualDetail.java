package imlv.vn.real.models;

import net.java.ao.Entity;

public interface ManualDetail extends Entity {

    int getManualVersionKey();

    void setManualVersionKey(int ManualVersionKey);

    String getManualVersion();

    void setManualVersion(String ManualVersion);

    Long getPageID();

    void setPageID(Long PageID);

    int getPageVersion();

    void setPageVersion(int PageVersion);

    String getPageTitle();

    void setPageTitle(String PageTitle);

    int getParentPageID();

    void setParentPageID(int ParentPageID);

    int getPosition();

    void setPosition(int Position);

    boolean isDeleteFlag();

    void setDeleteFlag(boolean DeleteFlag);
}
