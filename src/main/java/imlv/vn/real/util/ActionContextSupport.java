package imlv.vn.real.util;

import com.opensymphony.xwork.ActionContext;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ActionContextSupport {

    public ActionContextSupport() {
    }

    public static String getParameterValue(ActionContext context, String key) {
        Object paramValues = context.getParameters().get(key);
        if (paramValues instanceof String[] && ((String[]) paramValues).length != 0) {
            return ((String[]) paramValues)[0];
        } else if (paramValues instanceof String) {
            return (String) paramValues;
        }
        return null;
    }

    public static String[] getParameterValues(ActionContext context, String key) {
        Object paramValues = context.getParameters().get(key);
        if (paramValues instanceof String[] && ((String[]) paramValues).length != 0) {
            return (String[]) paramValues;
        }
        return new String[0];
    }

    public static String getNow() {
        return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
    }

}
