package imlv.vn.real.services;

import com.atlassian.activeobjects.external.ActiveObjects;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import imlv.vn.real.models.ManualAttachment;
import java.util.List;
import javax.inject.Inject;
import net.java.ao.Query;

public class ManualAttachmentServiceImpl implements ManualAttachmentService {

    private final ActiveObjects ao;

    @Inject
    public ManualAttachmentServiceImpl(ActiveObjects ao) {
        this.ao = checkNotNull(ao);
    }

    @Override
    public List<ManualAttachment> getAll() {
        return newArrayList(ao.find(ManualAttachment.class, Query.select().where("Delete_Flag = ?", false)));
    }

//    @Override
//    public List<ManualAttachment> search(String searchKey) {
//        try {
//            StringBuilder sql = new StringBuilder();
//            sql.append(sql);
//            sql.append(sql);
//            sql.append(sql);
//            sql.append(sql);
//
//            return newArrayList(ao.find(ManualAttachment.class, Query.select().
//                    where("MANUAL_VERSION_KEY Like ? or ManualVersion Like ? or PageID Like ? "
//                            + "or PageVersion Like ? or AttachmentID Like ? or AttachmentVersion Like ?",
//                            "%" + searchKey + "%", "%" + searchKey + "%", "%" + searchKey + "%",
//                            "%" + searchKey + "%", "%" + searchKey + "%", "%" + searchKey + "%")));
//        } catch (Exception e) {
//            throw new RuntimeException();
//        }
//    }

    @Override
    public ManualAttachment add(int manualVersionKey, String manualVersion, long pageID, int pageVersion, long attachmentID, int attachmentVersion) {
        try {
            final ManualAttachment manualAttachment = ao.create(ManualAttachment.class);
            manualAttachment.setManualVersionKey(manualVersionKey);
            manualAttachment.setManualVersion(manualVersion);
            manualAttachment.setPageID(pageID);
            manualAttachment.setPageVersion(pageVersion);
            manualAttachment.setAttachmentID(attachmentID);
            manualAttachment.setAttachmentVersion(attachmentVersion);
            manualAttachment.setDeleteFlag(false);
            manualAttachment.save();
            return manualAttachment;
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

    @Override
    public ManualAttachment edit(ManualAttachment manualAttachment) {
        try {
            final ManualAttachment manualAttachmentUpdate = ao.get(ManualAttachment.class, manualAttachment.getManualVersionKey());
            manualAttachmentUpdate.setManualVersionKey(manualAttachment.getManualVersionKey());
            manualAttachmentUpdate.setManualVersion(manualAttachment.getManualVersion());
            manualAttachmentUpdate.setPageID(manualAttachment.getPageID());
            manualAttachmentUpdate.setPageVersion(manualAttachment.getPageVersion());
            manualAttachmentUpdate.setAttachmentID(manualAttachment.getAttachmentID());
            manualAttachmentUpdate.setAttachmentVersion(manualAttachment.getAttachmentVersion());
            manualAttachmentUpdate.setDeleteFlag(manualAttachment.isDeleteFlag());
            manualAttachmentUpdate.save();
            return null;
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

    @Override
    public void delete(ManualAttachment manualAttachment) {
        try {
            ao.delete(manualAttachment);
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

}
