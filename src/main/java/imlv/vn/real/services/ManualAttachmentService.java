package imlv.vn.real.services;

import imlv.vn.real.models.ManualAttachment;
import java.util.List;

public interface ManualAttachmentService {

    List<ManualAttachment> getAll();

//    List<ManualAttachment> search(String searchKey);

    ManualAttachment add(int manualVersionKey, String manualVersion,long pageID, int pageVersion,long attachmentID, int attachmentVersion);

    ManualAttachment edit(ManualAttachment manualAttachment);

    void delete(ManualAttachment manualAttachment);
}
