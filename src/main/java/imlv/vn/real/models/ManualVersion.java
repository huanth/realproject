package imlv.vn.real.models;

import net.java.ao.Entity;

public interface ManualVersion extends Entity {

    int getManualVersionKey();

    void setManualVersionKey(int ManualVersionKey);

    String getManualVersion();

    void setManualVersion(String ManualVersion);

    String getManualVersionDisplay();

    void setManualVersionDisplay(String ManualVersionDisplay);

    String getSpaceKey();

    void setSpaceKey(String SpaceKey);

    boolean isStatus();

    void setStatus(boolean Status);

    boolean isDeleteFlag();

    void setDeleteFlag(boolean DeleteFlag);
}
