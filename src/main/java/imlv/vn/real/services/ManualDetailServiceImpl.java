package imlv.vn.real.services;

import com.atlassian.activeobjects.external.ActiveObjects;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import imlv.vn.real.models.ManualDetail;
import java.util.List;
import javax.inject.Inject;
import net.java.ao.Query;

public class ManualDetailServiceImpl implements ManualDetailService {

    private final ActiveObjects ao;

    @Inject
    public ManualDetailServiceImpl(ActiveObjects ao) {
        this.ao = checkNotNull(ao);
    }

    @Override
    public List<ManualDetail> getAll() {
        return newArrayList(ao.find(ManualDetail.class, Query.select().where("Delete_Flag = ?", false)));
    }

    @Override
    public ManualDetail add(int manualVersionKey, String manualVersion, long pageID, int pageVersion, String pageTitle, int parentPageID, int position) {
        final ManualDetail manualDetail = ao.create(ManualDetail.class);
        manualDetail.setManualVersionKey(manualVersionKey);
        manualDetail.setManualVersion(manualVersion);
        manualDetail.setPageID(pageID);
        manualDetail.setPageVersion(pageVersion);
        manualDetail.setPageTitle(pageTitle);
        manualDetail.setParentPageID(parentPageID);
        manualDetail.setPosition(position);
        manualDetail.setDeleteFlag(false);
        manualDetail.save();
        return manualDetail;
    }

    @Override
    public ManualDetail edit(ManualDetail manualDetail) {
        try {
            final ManualDetail manualDetailUpdate = ao.get(ManualDetail.class, manualDetail.getManualVersionKey());
            manualDetailUpdate.setManualVersionKey(manualDetail.getManualVersionKey());
            manualDetailUpdate.setManualVersion(manualDetail.getManualVersion());
            manualDetailUpdate.setPageID(manualDetail.getPageID());
            manualDetailUpdate.setPageVersion(manualDetail.getPageVersion());
            manualDetailUpdate.setPageTitle(manualDetail.getPageTitle());
            manualDetailUpdate.setParentPageID(manualDetail.getParentPageID());
            manualDetailUpdate.setPosition(manualDetail.getPosition());
            manualDetailUpdate.setDeleteFlag(manualDetail.isDeleteFlag());
            manualDetailUpdate.save();
            return null;
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

    @Override
    public void delete(ManualDetail manualDetail) {
        try {
            ao.delete(manualDetail);
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

}
