package imlv.vn.real.services;

import imlv.vn.real.models.ManualHistory;
import java.util.List;

public interface ManualHistoryService {

    List<ManualHistory> getAll();

//    List<ManualHistory> search(String searchKey);

    ManualHistory add(int manualVersionKey, String baseManualVersion, long basePageID, int basePageVersion, String fromManualVersion, long fromPageID, int fromPageVersion,String toManualVersion,long toPageID, int toPageVersion);

    ManualHistory edit(ManualHistory manualHistory);

    void delete(ManualHistory manualHistory);
}
