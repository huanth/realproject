package imlv.vn.real.models;

import net.java.ao.RawEntity;
import net.java.ao.schema.AutoIncrement;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.PrimaryKey;

public interface ManualManagement extends RawEntity<Integer> {

    @AutoIncrement
    @NotNull
    @PrimaryKey
    int getManualVersionKey();

    void setManualVersionKey(int ManualVersionKey);

    String getManualVersionName();

    void setManualVersionName(String ManualVersionName);

    String getProductName();

    void setProductName(String ProductName);

    String getCategory();

    void setCategory(String Category);

    boolean isDeleteFlag();

    void setDeleteFlag(boolean DeleteFlag);
}
