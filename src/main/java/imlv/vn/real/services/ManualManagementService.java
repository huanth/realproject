package imlv.vn.real.services;

import com.atlassian.activeobjects.tx.Transactional;
import imlv.vn.real.models.ManualManagement;
import java.util.List;

@Transactional
public interface ManualManagementService {

    List<ManualManagement> getAll();

//    List<ManualManagement> search(String searchKey);

    List<ManualManagement> searchByCategory(String searchKey);

    int add(String manualVersionName, String productName, String categoryName);

    ManualManagement edit(ManualManagement manualManagement);

    void delete(ManualManagement manualManagement);
}
