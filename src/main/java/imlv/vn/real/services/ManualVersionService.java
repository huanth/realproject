package imlv.vn.real.services;

import imlv.vn.real.models.ManualVersion;
import java.util.List;

public interface ManualVersionService {

    List<ManualVersion> getAll();

    ManualVersion add(int manualVersionKey, String manualVersion, String manualVersionDisplay, String SpaceKey);

    ManualVersion edit(ManualVersion manualVersion);

    void delete(ManualVersion manualVersion);
}
