/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imlv.vn.real.services;

import imlv.vn.real.models.ManualIncludePage;
import java.util.List;

/**
 *
 * @author huanth
 */
public interface ManualIncludePageService {

    List<ManualIncludePage> getAll();

//    List<ManualIncludePage> search(String searchKey);

    ManualIncludePage add(int manualVersionKey, String manualVersion,long pageID, int pageVersion, long includePageID, int includePageVersion);

    ManualIncludePage edit(ManualIncludePage manualIncludePage);

    void delete(ManualIncludePage manualIncludePage);
}
