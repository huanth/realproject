package imlv.vn.real.services;

import com.atlassian.activeobjects.external.ActiveObjects;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import imlv.vn.real.models.ManualHistory;
import imlv.vn.real.models.ManualVersion;
import java.util.List;
import javax.inject.Inject;
import net.java.ao.Query;

public class ManualHistoryServiceImpl implements ManualHistoryService {
    
    private final ActiveObjects ao;
    
    @Inject
    public ManualHistoryServiceImpl(ActiveObjects ao) {
        this.ao = checkNotNull(ao);
    }
    
    @Override
    public List<ManualHistory> getAll() {
        return newArrayList(ao.find(ManualHistory.class, Query.select().where("Delete_Flag = ?", false)));
        
    }
    
    @Override
    public ManualHistory add(int manualVersionKey, String baseManualVersion, long basePageID, int basePageVersion, String fromManualVersion, long fromPageID, int fromPageVersion, String toManualVersion, long toPageID, int toPageVersion) {
        try {
            final ManualHistory manualHistory = ao.create(ManualHistory.class);
            manualHistory.setManualVersionKey(manualVersionKey);
            manualHistory.setBaseManualVersion(baseManualVersion);
            manualHistory.setBasePageID(basePageID);
            manualHistory.setBasePageVersion(basePageVersion);
            manualHistory.setFromManualVersion(fromManualVersion);
            manualHistory.setFromPageID(fromPageID);
            manualHistory.setFromPageVersion(fromPageVersion);
            manualHistory.setToManualVersion(toManualVersion);
            manualHistory.setToPageID(toPageID);
            manualHistory.setToPageVersion(toPageVersion);
            manualHistory.setDeleteFlag(false);
            manualHistory.save();
            return manualHistory;
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
    
    @Override
    public ManualHistory edit(ManualHistory manualHistory) {
        try {
            final ManualHistory manualHistoryUpdate = ao.get(ManualHistory.class, manualHistory.getManualVersionKey());
            manualHistoryUpdate.setManualVersionKey(manualHistory.getManualVersionKey());
            manualHistoryUpdate.setBaseManualVersion(manualHistory.getBaseManualVersion());
            manualHistoryUpdate.setBasePageID(manualHistory.getBasePageID());
            manualHistoryUpdate.setBasePageVersion(manualHistory.getBasePageVersion());
            manualHistoryUpdate.setFromManualVersion(manualHistory.getFromManualVersion());
            manualHistoryUpdate.setFromPageID(manualHistory.getFromPageID());
            manualHistoryUpdate.setFromPageVersion(manualHistory.getFromPageVersion());
            manualHistoryUpdate.setToManualVersion(manualHistory.getToManualVersion());
            manualHistoryUpdate.setToPageID(manualHistory.getToPageID());
            manualHistoryUpdate.setToPageVersion(manualHistory.getToPageVersion());
            manualHistoryUpdate.setDeleteFlag(manualHistory.isDeleteFlag());
            manualHistoryUpdate.save();
            return null;
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
    
    @Override
    public void delete(ManualHistory manualHistory) {
        try {
            ao.delete(manualHistory);
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
    
}
