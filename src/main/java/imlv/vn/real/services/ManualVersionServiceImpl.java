package imlv.vn.real.services;

import com.atlassian.activeobjects.external.ActiveObjects;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import imlv.vn.real.models.ManualVersion;
import java.util.List;
import javax.inject.Inject;
import net.java.ao.Query;

public class ManualVersionServiceImpl implements ManualVersionService {

    private final ActiveObjects ao;

    @Inject
    public ManualVersionServiceImpl(ActiveObjects ao) {
        this.ao = checkNotNull(ao);
    }

    @Override
    public List<ManualVersion> getAll() {
        return newArrayList(ao.find(ManualVersion.class, Query.select().where("Delete_Flag = ?", false).order("Manual_Version DESC")));
    }

    @Override
    public ManualVersion add(int manualVersionKey, String manualVersionName, String manualVersionDisplay, String SpaceKey) {
        final ManualVersion manualVersion = ao.create(ManualVersion.class);
        manualVersion.setManualVersionKey(manualVersionKey);
        manualVersion.setManualVersion(manualVersionName);
        manualVersion.setManualVersionDisplay(manualVersionDisplay);
        manualVersion.setSpaceKey(SpaceKey);
        manualVersion.setStatus(false);
        manualVersion.setDeleteFlag(false);
        manualVersion.save();
        return manualVersion;
    }

    @Override
    public ManualVersion edit(ManualVersion manualVersion) {
        List<ManualVersion> listManualVersion = newArrayList(ao.find(ManualVersion.class, Query.select().where("Manual_Version_Key = ? AND Manual_Version = ?", manualVersion.getManualVersionKey(), manualVersion.getManualVersion())));
        final ManualVersion manualVersionUpdate = listManualVersion.get(0);
        manualVersionUpdate.setManualVersion(manualVersion.getManualVersion());
        manualVersionUpdate.setManualVersionDisplay(manualVersion.getManualVersionDisplay());
        manualVersionUpdate.setSpaceKey(manualVersion.getSpaceKey());
        manualVersionUpdate.setStatus(manualVersion.isStatus());
        manualVersionUpdate.setDeleteFlag(manualVersion.isDeleteFlag());
        manualVersionUpdate.save();
        return null;
    }

    @Override
    public void delete(ManualVersion manualVersion) {
        ao.delete(manualVersion);
    }

}
