package imlv.vn.real.factory;

import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.SpaceManager;
import imlv.vn.real.services.ManualAttachmentService;
import imlv.vn.real.services.ManualDetailService;
import imlv.vn.real.services.ManualHistoryService;
import imlv.vn.real.services.ManualIncludePageService;
import imlv.vn.real.services.ManualManagementService;
import imlv.vn.real.services.ManualVersionService;

public class ManualVersionFactory {

    private static ManualManagementService manualManagementService;
    private static ManualVersionService manualVersionService;
    private static ManualAttachmentService manualAttachmentService;
    private static ManualIncludePageService manualIncludePageService;
    private static ManualDetailService manualDetailService;
    private static ManualHistoryService manualHistoryService;
    private static SpaceManager spaceManager;
    private static PageManager pageManager;

    public ManualVersionFactory(ManualManagementService manualManagementService, ManualVersionService manualVersionService, ManualDetailService manualDetailService, ManualIncludePageService manualIncludePageService, ManualAttachmentService manualAttachmentService, ManualHistoryService manualHistoryService, SpaceManager spaceManager, PageManager pageManager) {
        ManualVersionFactory.manualManagementService = manualManagementService;
        ManualVersionFactory.manualVersionService = manualVersionService;
        ManualVersionFactory.manualDetailService = manualDetailService;
        ManualVersionFactory.manualIncludePageService = manualIncludePageService;
        ManualVersionFactory.manualAttachmentService = manualAttachmentService;
        ManualVersionFactory.manualHistoryService = manualHistoryService;
        ManualVersionFactory.spaceManager = spaceManager;
        ManualVersionFactory.pageManager = pageManager;
    }

    public static ManualManagementService getManualManagementService() {
        return manualManagementService;
    }

    public static void setManualManagementService(ManualManagementService manualManagementService) {
        ManualVersionFactory.manualManagementService = manualManagementService;
    }

    public static ManualVersionService getManualVersionService() {
        return manualVersionService;
    }

    public static void setManualVersionService(ManualVersionService manualVersionService) {
        ManualVersionFactory.manualVersionService = manualVersionService;
    }

    public static ManualAttachmentService getManualAttachmentService() {
        return manualAttachmentService;
    }

    public static void setManualAttachmentService(ManualAttachmentService manualAttachmentService) {
        ManualVersionFactory.manualAttachmentService = manualAttachmentService;
    }

    public static ManualIncludePageService getManualIncludePageService() {
        return manualIncludePageService;
    }

    public static void setManualIncludePageService(ManualIncludePageService manualIncludePageService) {
        ManualVersionFactory.manualIncludePageService = manualIncludePageService;
    }

    public static ManualDetailService getManualDetailService() {
        return manualDetailService;
    }

    public static void setManualDetailService(ManualDetailService manualDetailService) {
        ManualVersionFactory.manualDetailService = manualDetailService;
    }

    public static ManualHistoryService getManualHistoryService() {
        return manualHistoryService;
    }

    public static void setManualHistoryService(ManualHistoryService manualHistoryService) {
        ManualVersionFactory.manualHistoryService = manualHistoryService;
    }

    public static SpaceManager getSpaceManager() {
        return spaceManager;
    }

    public static void setSpaceManager(SpaceManager spaceManager) {
        ManualVersionFactory.spaceManager = spaceManager;
    }

    public static PageManager getPageManager() {
        return pageManager;
    }

    public static void setPageManager(PageManager pageManager) {
        ManualVersionFactory.pageManager = pageManager;
    }

}
