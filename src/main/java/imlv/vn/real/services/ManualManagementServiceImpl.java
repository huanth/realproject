package imlv.vn.real.services;

import com.atlassian.activeobjects.external.ActiveObjects;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import imlv.vn.real.models.ManualManagement;
import java.util.List;
import javax.inject.Inject;
import net.java.ao.Query;

public class ManualManagementServiceImpl implements ManualManagementService {

    private final ActiveObjects ao;

    @Inject
    public ManualManagementServiceImpl(ActiveObjects ao) {
        this.ao = checkNotNull(ao);
    }

    @Override
    public List<ManualManagement> getAll() {
        return newArrayList(ao.find(ManualManagement.class, Query.select().where("Delete_Flag = ?", false).order("Manual_Version_Name ASC")));
    }

    @Override
    public List<ManualManagement> searchByCategory(String searchKey) {
        return newArrayList(ao.find(ManualManagement.class, Query.select().where("CATEGORY LIKE ? and Delete_Flag = ?", "%" + searchKey + "%", false).order("Manual_Version_Name ASC")));
    }

    @Override
    public int add(String manualVersionName, String productName, String categoryName) {
        try {
            final ManualManagement manualManagement = ao.create(ManualManagement.class);
            manualManagement.setManualVersionName(manualVersionName);
            manualManagement.setProductName(productName);
            manualManagement.setCategory(categoryName);
            manualManagement.setDeleteFlag(false);
            manualManagement.save();
            return manualManagement.getManualVersionKey();
        } catch (Exception e) {
            return -1;
        }
    }

    @Override
    public ManualManagement edit(ManualManagement manualManagement) {
        try {
            final ManualManagement manualManagementUpdate = ao.get(ManualManagement.class, manualManagement.getManualVersionKey());
            manualManagementUpdate.setManualVersionName(manualManagement.getManualVersionName());
            manualManagementUpdate.setProductName(manualManagement.getProductName());
            manualManagementUpdate.setCategory(manualManagement.getCategory());
            manualManagementUpdate.setDeleteFlag(manualManagement.isDeleteFlag());
            manualManagementUpdate.save();
            return null;
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

    @Override
    public void delete(ManualManagement manualManagement) {
        try {
            ao.delete(manualManagement);
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
}
