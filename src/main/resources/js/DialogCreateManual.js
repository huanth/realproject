/* global AJS */
AJS.toInit(function () {
    // Shows the dialog when the "Show dialog" button is clicked
    AJS.$("#dialog-show-button").on('click', function (e) {
        e.preventDefault();
        $('#lbl-error').html('');
        $('#categories').val('');
        $('#manual-space-content input').val('');
        $('a.mycategories:hidden').show();
        $('#add-category-table td#category-choose div#categories-was-choosen').html('');
        AJS.dialog2("#manual-space-dialog").show();
    });

    // Hides the dialog
    AJS.$("#manual-space-cancel-button").on('click', function (e) {
        e.preventDefault();
        AJS.dialog2("#manual-space-dialog").hide();
    });

    // Hide event - this is triggered when the dialog is hidden
    AJS.dialog2("#manual-space-dialog").on("hide", function () {
    });

    AJS.dialog2("#add-category-dialog").on("hide", function () {
    });

    AJS.$("#add-category-cancel-button").on('click', function () {
        AJS.dialog2("#manual-space-dialog").hide();
        AJS.dialog2("#add-category-dialog").hide();
    });

    $('#dialog-back-button').on('click', function () {
        AJS.dialog2("#add-category-dialog").hide();
        AJS.dialog2("#manual-space-dialog").show();
    });

    $('#manual-space-next-button').click(function (e) {
        if ($('#manual-name').val().length > 0) {
            $.ajax({
                async: false,
                type: 'GET',
                url: AJS.contextPath() + "/plugins/real/load-category.action",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: {},
                success: function (returnData) {
                    if ($('.dialog-categories-management-conten #category-haven a').length === 0) {
                        var categories = '';
                        $.each(returnData.categories, function (index, value) {
                            categories += ' <a style="word-break: break-word;" class="mycategories">' + value + '</a> ';
                        });
                        $('.dialog-categories-management-conten #category-haven\n\
\n\
\n\
').append(categories);
                    }
                }
            });
            AJS.dialog2("#manual-space-dialog").hide();
            AJS.dialog2("#add-category-dialog").show();
        } else {
            e.preventDefault();
            $('#lbl-error').html('<font color="red">マニュアル名を入力してください。</font>');
        }
    });

    $('#add-category-table td#category-haven').on('click', 'a.mycategories', function () {
        var add_category = '<span id="split-label" class="aui-label aui-label-closeable aui-label-split"><a style="word-break: break-word;" class="aui-label-split-main" href="#">' +
                $(this).text() +
                '</a><span class="aui-label-split-close"><span tabindex="0" class="aui-icon aui-icon-close" original-title="(remove splitLabel)">' +
                '</span></span></span>';
        $('#add-category-table #category-choose #categories-was-choosen').append(add_category);
        $(this).hide();
    });

    $('#choose-category-button').click(function (e) {
        var category = $('#categories').val();
        var add_category = '';
        var flag = true;
        if (category.length > 0) {
            $.each($('#category-choose #categories-was-choosen #split-label'), function (index, value) {
                if (category === $(this).text()) {
                    flag = false;
                }
            });
            $.each($('#add-category-table td#category-haven a.mycategories'), function (index, value) {
                if (category === $(this).text()) {
                    flag = false;
                    $(this).trigger('click');
                }
            });
            if (flag === true) {
                add_category = '<span id="split-label" class="aui-label aui-label-closeable aui-label-split"><a style="word-break: break-word;" class="aui-label-split-main" href="#">'
                        + category +
                        '</a><span class="aui-label-split-close"><span tabindex="0" class="aui-icon aui-icon-close" original-title="(remove splitLabel)">' +
                        '</span></span></span>';
                $('#add-category-table #category-choose #categories-was-choosen').append(add_category);
            }
        }
        $('#categories').val('');
    });

    $('#add-category-table td#category-choose div#categories-was-choosen').on('click', 'span.aui-icon-close', function () {
        $(this).parent().parent().remove();
        var curCategory = $(this).parent().parent().text();
        $.each($('a.mycategories:hidden'), function (index, value) {
            var hiddenCategory = $(this).text();
            if (curCategory === hiddenCategory) {
                $(this).show();
            }
        });
    });

    $('#submit-create-space-button').click(function (e) {
        $('#manualname').val($('#manual-name').val());
        $('#productname').val($('#product-name').val());
        $('#versiondisplay').val($('#version-display').val());
        var transffer = $('#add-category-table td#category-choose div#categories-was-choosen span#split-label');
        var cateStr = '';
        $.each(transffer, function (index, value) {
            cateStr += '<input type="hidden" name="createcategories" value="' + $(this).text() + '" />';
        });
        $('#form-submit-create-space').append(cateStr);
        AJS.dialog2("#add-category-dialog").hide();
        $('#form-submit-create-space').trigger('submit');
    });

    $('#form-submit-create-space').submit(function () {
    });

    $('#manual-management-table-tbody').on('click', 'a.link-to-home-page', function () {
        var urlRedirect;
        $.each($(this).parent().parent().nextUntil('tr.table-manual-management-table-tr'), function (index, value) {
            urlRedirect = $(this).find('input.temp-link-to-home-page').val();
            return;
        });
        window.location.href = urlRedirect + "?key=";
    });
});


